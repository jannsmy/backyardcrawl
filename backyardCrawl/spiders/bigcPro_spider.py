# -*- coding: utf-8 -*-

import os
import time
import datetime
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
import scrapy

class bigcProSpider(Spider):
	
	name = "bigc_promotion"

	def __init__(self):

		concurrent_requests = 30
		self.date = str(int(time.time()))

		self.mapping = { "https://www.bigc.co.th/promotion/super-save-promo-bro16.html" : "สินค้าถูกสุด ๆ ไปเลย",
						"https://www.bigc.co.th/promotion/buy1-get1-free-promo-bro16.html" : "ซื้อ 1 แถม 1",
						"https://www.bigc.co.th/promotion/buy2-get1-free-promo-bro16.html" : "ซื้อ 2 แถม 1",
						"https://www.bigc.co.th/promotion/buy3-get1-free-promo-bro16.html" : "ซื้อ 3 แถม 1",
						"https://www.bigc.co.th/promotion/buy2-cheaper-promo-bro16.html" : "ซื้อ 2 ชิ้นถูกกว่า" }


		self.start_urls     = self.mapping.keys()

	def parse(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		urls = set()
		path_url = response.url + "?p="
		urls.add(path_url + "1")
		page_no = None

		if soup.find_all( "div", { "class" : "pages-content" }): 
			footer =  soup.find_all( "div", { "class" : "pages-content" })[0]
			if footer.find_all( "a", { "class" : "last" }):
				page_no = footer.find_all( "a", { "class" : "last" })[0].get_text()
			elif footer.find_all( "a" ):
				for a in footer.find_all( "a" ):
					urls.add(a.get("href"))
		if page_no:
			for page in range(2, int(page_no) + 1):
				urls.add( path_url + str(page))
		
		promotion_type = self.mapping.get(response.url)
		
		for url in urls :
			yield scrapy.Request(url, callback = self.get_link_product, meta = {"promotion_type" : promotion_type})
		
	
	def get_link_product(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		file_error = open("./data/temp/error/bigc_pro_err.txt", "a")
		try :
			file = open("./data/temp/bigc_promotion.txt", 'a')
			if soup.find_all( "ul", { "class" : "products-grid products-grid--max-3-col" }):
				ul = soup.find_all( "ul", { "class" : "products-grid products-grid--max-3-col" })[0]
				if ul.find_all( "li"):
					for li in ul.find_all( "li") :
						link_product = None
						if li.find_all( "h2", { "class" : "product-name" }): 
							h2 = li.find_all( "h2", { "class" : "product-name" })[0]
							if h2.find_all( "a"): 
								link_product = h2.find_all( "a")[0].get("href")
							if link_product != "" :
								data = link_product + "|" + response.meta['promotion_type']
								file.write(data)
								file.write("\n")
		except :
			file_error.write(response.url + "\n")