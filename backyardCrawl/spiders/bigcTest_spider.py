# -*- coding: utf-8 -*-

import os
import time
import datetime
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
import scrapy

class bigcTestSpider(Spider):
	
	name = "bigc_url"

	def __init__(self):

		concurrent_requests = 30
		self.date = str(int(time.time()))
		
		self.start_urls     = [ "https://www.bigc.co.th/food.html",
								"https://www.bigc.co.th/beverages.html",
								"https://www.bigc.co.th/snacks-sweets.html",
								"https://www.bigc.co.th/health-beauty.html",
								"https://www.bigc.co.th/mom-baby-kids.html",
								"https://www.bigc.co.th/household.html",
								"https://www.bigc.co.th/home-appliances-electronic-products.html",
								"https://www.bigc.co.th/stationaries.html",
								"https://www.bigc.co.th/pet-outdoor-etc.html",
								"https://www.bigc.co.th/import-product.html" ]

		#self.start_urls     = [ "https://www.bigc.co.th/fresh-food.html" ]
		#self.start_urls     = [ "https://www.bigc.co.th/fresh-food/vegetable-fruit-fresh-flower/local-vegetable.html" ]

							
	def parse(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		if soup.find_all( "li", { "class" : "amshopby-cat level0 has-child" }): 
			for li in soup.find_all( "li", { "class" : "amshopby-cat level0 has-child" }): 
				for a in li.find_all( "a"):
					yield scrapy.Request(a.get("href"), callback=self.get_sub_category, meta= {"main_category" : a.get_text().encode('utf-8').strip()})

	def get_sub_category(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		if soup.find_all( "li", { "class" : "amshopby-cat level0" }): 
			file = open("./data/temp/bigc_url.txt", 'a')
			for li in soup.find_all( "li", { "class" : "amshopby-cat level0" }): 
				for a in li.find_all( "a"):
					file = open("./data/temp/bigc_url.txt", 'a')
					file.write(a.get("href") + "|" + response.meta['main_category'] + "|" + a.get_text().encode('utf-8').strip())
					file.write("\n")