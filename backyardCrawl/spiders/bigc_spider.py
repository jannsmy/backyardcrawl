# -*- coding: utf-8 -*-

import os
import time
import datetime
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
import scrapy

class bigcSpider(Spider):
	
	name = "bigc"
	count = 0

	def __init__(self):

		concurrent_requests = 30
		self.date = str(int(time.time()))
		
		self.start_urls     = [ "https://www.bigc.co.th/fresh-food.html",
								"https://www.bigc.co.th/food.html",
								"https://www.bigc.co.th/beverages.html",
								"https://www.bigc.co.th/snacks-sweets.html",
								"https://www.bigc.co.th/health-beauty.html",
								"https://www.bigc.co.th/mom-baby-kids.html",
								"https://www.bigc.co.th/household.html",
								"https://www.bigc.co.th/home-appliances-electronic-products.html",
								"https://www.bigc.co.th/stationaries.html",
								"https://www.bigc.co.th/pet-outdoor-etc.html",
								"https://www.bigc.co.th/pure.html",
								"https://www.bigc.co.th/besico.html",
								"https://www.bigc.co.th/import-product.html" ]
	
		
		#self.start_urls     = [ "https://www.bigc.co.th/food.html" ]
		file_promotion = open("./data/temp/bigc_promotion.txt", 'r')
		file_brand = open("./data/temp/bigc_brand.txt", 'r')
		promotions = file_promotion.readlines()
		brands = file_brand.readlines()
		
		self.promotion_map = dict()
		for promotion in promotions:	
			data = promotion.split("|")
			self.promotion_map[data[0]] = data[1].strip()
		self.count = 0
		self.brand_map = dict()
		for brand in brands:	
			data = brand.split("|")
			self.brand_map[data[0]] = data[1].strip()

		print(len(self.promotion_map))
		print(len(self.brand_map))

		#self.start_urls     = [ "https://www.bigc.co.th/fresh-food.html" ]
		#self.start_urls     = [ "https://www.bigc.co.th/fresh-food/vegetable-fruit-fresh-flower/local-vegetable.html" ]

							
	def parse(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		if soup.find_all( "li", { "class" : "amshopby-cat level0 has-child" }): 
			for li in soup.find_all( "li", { "class" : "amshopby-cat level0 has-child" }): 
				for a in li.find_all( "a"):
					#print(a.get("href"))
					yield scrapy.Request(a.get("href"), callback=self.get_sub_category, meta= {"main_category" : a.get_text().encode('utf-8')})

	def get_sub_category(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		if soup.find_all( "li", { "class" : "amshopby-cat level0" }): 
			for li in soup.find_all( "li", { "class" : "amshopby-cat level0" }): 
				for a in li.find_all( "a"):
					#print(a.get("href"))
					yield scrapy.Request(a.get("href"), callback=self.get_page, meta= {"sub_category" : a.get_text().encode('utf-8'), "main_category" : response.meta['main_category']} )
	
	def get_page(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		urls = set()
		path_url = response.url + "?p="
		urls.add(path_url + "1")
		page_no = None

		if soup.find_all( "div", { "class" : "pages-content" }): 
			footer =  soup.find_all( "div", { "class" : "pages-content" })[0]
			if footer.find_all( "a", { "class" : "last" }):
				page_no = footer.find_all( "a", { "class" : "last" })[0].get_text()
				# print "last page" , page_no
			elif footer.find_all( "a" ):
				for a in footer.find_all( "a" ):
					urls.add(a.get("href"))
		if page_no:
			for page in range(2, int(page_no) + 1):
				urls.add( path_url + str(page))
		
		# print (response.meta["sub_category"] + " -> " + str(len(urls)))
		
		for url in urls :
			yield scrapy.Request(url, callback = self.get_link_product, meta = {"sub_category" : response.meta["sub_category"], "main_category" : response.meta["main_category"]})
		
	
	def get_link_product(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		if soup.find_all( "ul", { "class" : "products-grid products-grid--max-3-col" }):
			ul = soup.find_all( "ul", { "class" : "products-grid products-grid--max-3-col" })[0]
			if ul.find_all( "li"):
				for li in ul.find_all( "li") :
					link_product = None
					normal_price = ""
					normal_price_unit = ""
					sub_price = ""
					sub_price_unit = ""
					if li.find_all( "h2", { "class" : "product-name" }): 
						h2 = li.find_all( "h2", { "class" : "product-name" })[0]
						if h2.find_all( "a"): 
							link_product = h2.find_all( "a")[0].get("href")

					if li.find_all("span", {"class":"price-follow_unit"}): 
						span = li.find_all("span", {"class":"price-follow_unit"})[0]
						if span.find_all("span", {"class":"price"}):
							sub_price = span.find_all("span", {"class":"price"})[0].get_text().encode('utf-8').strip().replace("฿", "")
						if span.find_all("span", {"class":"unit-product"}):
							sub_price_unit = span.find_all("span", {"class":"unit-product"})[0].get_text().encode('utf-8').strip().replace("/", "")

					if li.find_all("span", {"class":"group-price-base"}): 
						span = li.find_all("span", {"class":"group-price-base"})[0]
						if span.find_all("span", {"class":"price"}):
							normal_price = span.find_all("span", {"class":"price"})[0].get_text().encode('utf-8').strip().replace("฿", "")
						if span.find_all("span", {"class":"unit-product"}):
							normal_price_unit = span.find_all("span", {"class":"unit-product"})[0].get_text().encode('utf-8').strip().replace("/", "")

					if link_product :		
						yield scrapy.Request(link_product, callback=self.get_product,\
							meta = {"sub_category" : response.meta["sub_category"], "main_category" : response.meta["main_category"],
									"normal_price" : normal_price, "normal_price_unit" : normal_price_unit, 
									"sub_price" : sub_price, "sub_price_unit" : sub_price_unit })

	def get_product(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		file_error = open("./data/temp/error/bigc_err.txt", "a")
		try :
			main_category = response.meta['main_category']
			sub_category = response.meta['sub_category']
			product_id = ""
			product_name  = ""
			normal_price  = response.meta['normal_price']
			normal_price_unit = response.meta['normal_price_unit']
			sub_price = response.meta['sub_price']
			sub_price_unit = response.meta['sub_price_unit']
			promotion_price = ""
			promotion_price_unit = ""
			discount = ""
			promotion_date = ""
			if self.promotion_map.get(response.url) :
				promotion_type = self.promotion_map.get(response.url)
			else :
				promotion_type = ""
			if self.brand_map.get(response.url) :
				own_brand = self.brand_map.get(response.url)
			else :
				own_brand = ""
			link = response.url
			sale_count = ""

			# product_id + "|" + product_name + "|" + main_category + "|" + sub_category + "|" + normal_price + "|" + normal_price_unit + "|" + sub_price + "|" + sub_price_unit + "|" + promotion_price + "|" + promotion_price_unit + "|" + discount + "|" + promotion_date + "|" + promotion_type + "|" + own_brand + "|" + self.date + "|" + link + "|" + sale_count
			
			# product_id
			if soup.find_all("span", {"class":"sku-product"}): 
				product_id = soup.find_all("span", {"class":"sku-product"})[0].get_text().encode('utf-8').replace("รหัสสินค้า: ", "")
			
			# product_name
			if soup.find_all("h1", {"class":"h1"}): 
				product_name = soup.find_all("h1", {"class":"h1"})[0].get_text().encode('utf-8')

			# normal_price
			if normal_price == "" :
				if soup.find_all("p", {"class":"old-price"}): 
					p = soup.find_all("p", {"class":"old-price"})[0]
					if p.find_all("span", {"class":"price"}):
						normal_price = p.find_all("span", {"class":"price"})[0].get_text().encode('utf-8').strip().replace("฿", "")
				elif soup.find_all("div", {"class":"regular-price"}): 
					div = soup.find_all("div", {"class":"regular-price"})[0]
					if div.find_all("span", {"class":"price"}):
						normal_price = div.find_all("span", {"class":"price"})[0].get_text().encode('utf-8').strip().replace("฿", "")

			# promotion_price
			if soup.find_all("p", {"class":"special-price"}): 
				p = soup.find_all("p", {"class":"special-price"})[0]
				if p.find_all("span", {"class":"price"}):
					promotion_price = p.find_all("span", {"class":"price"})[0].get_text().encode('utf-8').strip().replace("฿", "")

			# discount
			if soup.find_all("span", {"class":"save-price"}):
				discount = soup.find_all("span", {"class":"save-price"})[0].get_text().encode('utf-8').strip().replace("฿", "")

			# promotion_date
			if soup.find_all("div", {"class":"message-time-deal"}):
				promotion_date = soup.find_all("div", {"class":"message-time-deal"})[0].get_text().encode('utf-8')

			print (product_id + "|" + product_name + "|" + main_category + "|" + sub_category + "|" + normal_price + "|" + normal_price_unit + "|" + sub_price + "|" + sub_price_unit + "|" + promotion_price + "|" + promotion_price_unit + "|" + discount + "|" + promotion_date + "|" + promotion_type + "|" + own_brand + "|" + self.date + "|" + link + "|" + sale_count )
		
			file = open("./data/temp/bigc.txt", 'a')
			if product_id != "" :
				data = (product_id + "|" + product_name + "|" + main_category + "|" + sub_category + "|" + normal_price + "|" + normal_price_unit + "|" + sub_price + "|" + sub_price_unit + "|" + promotion_price + "|" + promotion_price_unit + "|" + discount + "|" + promotion_date + "|" + promotion_type + "|" + own_brand + "|" + self.date + "|" + link + "|" + sale_count )
				file.write(data)
				file.write("\n")
				self.count += 1
				print("========>"+str(self.count))
			else :
				file_error.write(response.url + "\n")
			
		except :
			file_error.write(response.url + "\n")
		