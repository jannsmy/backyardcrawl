# -*- coding: utf-8 -*-

import os
import json
import time
import datetime
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
import scrapy

class HappyFreahSpider(Spider):
	
	name = "happy"

	def __init__(self):

		concurrent_requests = 10
		self.date     		= str(int(time.time()))
		self.start_urls     = [ "https://www.happyfresh.co.th" ]
		self.duplicated		= set()
		
		file_category = open("./data/temp/happyfresh_category.txt", 'r')
		categories = file_category.readlines()		
		self.category_map = dict()
		self.sub_category_map = dict()
		for category in categories:	
			data = category.split("|")
			if data[3] not in self.category_map.keys():
				self.category_map[data[3]] = data[5].strip()
			self.sub_category_map[data[0]] = [ data[3], data[2].strip()]
	
	def parse(self, response):
		for category in self.category_map.keys():
			url = "https://gvg1d6u3wk.execute-api.ap-southeast-1.amazonaws.com/prod/catalog/stock_locations/100/taxons/{}/products?=undefined&page=1&taxon_id={}&popular=true&per_page=50".format(category, category)
			yield scrapy.Request(url, callback = self.get_page, method = 'GET', headers = {'X-API-KEY': 'HdI3wa6E3L6ECd1XYZZjJ92d4wUGOD4X6CrtO6MM'}, meta = {'taxon': category }) 
	
	def get_page(self, response):
		data = json.loads(response.body)
		page = data.get("pages")
		url = "https://gvg1d6u3wk.execute-api.ap-southeast-1.amazonaws.com/prod/catalog/stock_locations/100/taxons/{}/products?=undefined&page=1&taxon_id={}&popular=true&per_page=50"
		if data.get("pages") != 0:
			for i in range(1, page+1):
				url = response.url.split("&page=")[0] + "&page=" + str(i) + "&taxon_id="+ response.url.split("&page=")[1].split("&taxon_id=")[1]
				yield scrapy.Request(url, callback = self.get_content, method = 'GET', headers = {'X-API-KEY': 'HdI3wa6E3L6ECd1XYZZjJ92d4wUGOD4X6CrtO6MM', 'Locale':'th'}, meta = {'taxon' : response.meta['taxon']}) 

	def get_content(self, response):
		data = json.loads(response.body)
		products = data.get("products")
		sell_count = ""
		file = open("./data/temp/happyFresh.txt", 'a')
		for product in products :
			product_id = str(product.get("variants")[0].get("sku")).encode('utf-8').replace("-TH","")
			if product_id not in self.duplicated :
				#try :
				product_name = product.get("name").encode('utf-8')
				main_category_name = self.category_map.get(response.meta['taxon'])
				sub_category_name = self.get_sub_category_name(product.get("taxon_ids"))
				normal_price = str(product.get("normal_price"))
				normal_price_unit = str(product.get("display_unit").encode('utf-8'))
				percentage_discount = str(product.get("display_promo_price_percentage").encode('utf-8'))
				if percentage_discount != "" :
					promotion_price = str(product.get("price"))
					promotion_price_unit = str(product.get("display_unit").encode('utf-8'))
				else :
					promotion_price = ""
					promotion_price_unit = ""
				api_link = response.url
				unit_cost_price = str(product.get("display_supermarket_unit_cost_price").encode('utf-8')).replace("฿", "").split(" / ")
				sub_price = unit_cost_price[0]
				sub_price_unit = unit_cost_price[1]

					
				data = product_id + "|" + product_name + "|" +  main_category_name + "|" + sub_category_name + "|" +normal_price + "|" +  normal_price_unit + "|" + sub_price + "|" +  sub_price_unit + "|" + promotion_price  + "|" +  promotion_price_unit + "|" +  percentage_discount + "|" +  self.date + "|" +  api_link + "|" + sell_count
				file.write(data)
				file.write("\n")
				self.duplicated.add(product_id)
				'''
				except : 
					file_err = open("./data/temp/happyFresh_err.txt", 'a')
					file_err.write(str(response.meta['taxon']) + "|" + product_id + "|" + response.url)
					file_err.write("\n")
				'''

	def get_sub_category_name(self, taxon_ids):
		sub_name = list()
		for sub_taxon in taxon_ids:
			sub_taxon = str(sub_taxon)
			if sub_taxon in self.sub_category_map.keys():
				sub_name.append(self.sub_category_map.get(sub_taxon)[1])
		return ", ".join(sub_name)

				
				