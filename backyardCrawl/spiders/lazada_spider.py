# -*- coding: utf-8 -*-

import os
import json
import re
import csv
import time
import datetime
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
import scrapy

#SYMBOLS = r"[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]"
class lazadaSpider(Spider):
	
	name = "lazada"

	def __init__(self):
		domains             = "https://www.lazada.co.th"
		allowed_domains     = [domains]		
		download_delay      = 0.50
		concurrent_requests = 10
		self.start_urls     		= list()
		self.map_cate     	= dict()
		self.date     		= str(int(time.time()))
		f = open("start_url_lazada.txt", 'r')
		s = f.readlines()
		
		self.map = dict()
		for i in s:
			a = i.split("|")
			main = a[0]
			sub_cate = a[1]
			url = a[2]
			max_page = a[3]
			key = url.split("/?")[0]
			self.map[key] = { "main_category": main, "sub_category": sub_cate}
			for j in range(1, int(max_page) + 1):
				self.start_urls.append(url + str(j))
		
		print self.start_urls
		print len(self.start_urls)
		print self.map

		'''
		head = "https://www.lazada.co.th/shop-groceries-beverages-uht-milk-milk-powder/?spm=a2o4m.home.cate_6_2.4.1125515ffAui8H&page="
		for i in range(10,20):
			self.start_urls.append(head + str(i))
		print(self.start_urls)
		'''

	def parse(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		print response.url
		if soup.find_all("script", {"type":"application/ld+json"}): 
			for content in soup.find_all("script", {"type":"application/ld+json"}): 
				a = json.loads(content.getText())
				if a.get("@type") == "ItemList" :
					for i in a.get("itemListElement"):
						if response.url.split('/?')[0] in self.map.keys():
							self.map_cate[i.get("url")] = self.map.get(response.url.split('/?')[0])
						yield scrapy.Request(i.get("url"), callback=self.get_product)

	def get_product(self, response):
		main_category = ""
		sub_category = ""
		product_code = ""
		product_name = ""
		price = ""
		base_price = ""
		promotion_price = ""
		unit = ""
		sub_price = ""
		sale_count = ""
		soup = BeautifulSoup(response.body, "lxml")

		if soup.find_all("script", {"type":"application/ld+json"}): 
			for content in soup.find_all("script", {"type":"application/ld+json"}): 
				a = json.loads(content.getText())
				if a.get("@offer"):
					print a
		if response.url in self.map_cate.keys():
			main_category = self.map_cate.get(response.url).get("main_category")
			sub_category = self.map_cate.get(response.url).get("sub_category")

		if soup.find_all("div", {"class":"html-content key-value"}): 
			product_code = soup.find_all("div", {"class":"html-content key-value"})[1].get_text().encode('utf-8')

		if soup.find_all("span", {"class":"pdp-mod-product-badge-title"}): 
			product_name = soup.find_all("span", {"class":"pdp-mod-product-badge-title"})[0].get_text().encode('utf-8')
			
		if soup.find_all("span", {"class":" pdp-price pdp-price_type_normal pdp-price_color_orange pdp-price_size_xl"}): 
			price = soup.find_all("span", {"class":" pdp-price pdp-price_type_normal pdp-price_color_orange pdp-price_size_xl"})[0].get_text().encode('utf-8')

		if soup.find_all("span", {"class":" pdp-price pdp-price_type_deleted pdp-price_color_lightgray pdp-price_size_xs"}): 
			base_price = soup.find_all("span", {"class":" pdp-price pdp-price_type_deleted pdp-price_color_lightgray pdp-price_size_xs"})[0].get_text().encode('utf-8')

		if base_price != "":
			promotion_price = price
			price = base_price
		
		file = open("./data/temp/lazada.txt", 'a')
		file_error = open("./data/temp/error/lazada_err.txt", 'a')
		if product_code != "" :
			d = (product_code + "|" + product_name.strip() + "|" + main_category + "|" + sub_category + "|" + price + "|" + unit + "|" + sub_price + "|" + unit + "|" + promotion_price + "|" + unit + "|" + self.date + "|" + response.url + "|" + sale_count)
			file.write(d)
			file.write("\n")
		else :
			file_error.write(response.url + "\n")