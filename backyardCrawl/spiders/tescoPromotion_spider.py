# -*- coding: utf-8 -*-

import os
import time
import datetime
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
import scrapy

class TescoPromotionSpider(Spider):
	
	name = "tesco_promotion"

	def __init__(self):
		
		self.domains = "https://shoponline.tescolotus.com"
		concurrent_requests = 10
		self.date = str(int(time.time()))
		self.mapping = { "https://shoponline.tescolotus.com/groceries/th-TH/buy-lists/G00002946" : "สินค้าราคาพิเศษ",
						"https://shoponline.tescolotus.com/groceries/th-TH/buy-lists/G00002949" : "ซื้อ 2 ชิ้นประหยัดกว่า",
						"https://shoponline.tescolotus.com/groceries/th-TH/buy-lists/G00002948" : "ซื้อ 2 ฟรี 1",
						"https://shoponline.tescolotus.com/groceries/th-TH/buy-lists/G00002947" : "ซื้อ 1 ฟรี 1" }

		self.start_urls     = self.mapping.keys()
		self.count = 0
								
	def parse(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		urls = set()
		urls.add(response.url + "?page=1")
		
		if soup.find_all( "nav", { "class" : "pagination--page-selector-wrapper" }): 
			nav = soup.find_all( "nav", { "class" : "pagination--page-selector-wrapper" })[0]
			if nav.find_all( "li" ,{ "class" : "pagination-btn-holder" }):
				for li in nav.find_all( "li" ,{ "class" : "pagination-btn-holder" }):
					for a in li.find_all( "a" ) :
						if a.get("href") :
							url = self.domains + a.get("href")
							urls.add(url)
		promotion_type = self.mapping.get(response.url)	
		for url in urls:
			yield scrapy.Request(url, callback=self.get_link_product, meta = {"promotion_type" : promotion_type})
	
	
	def get_link_product(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		file_error = open("./data/temp/error/tesco_promotion_err.txt", "a")
		try :
			file = open("./data/temp/tesco_promotion.txt", 'a')
			if soup.find_all( "ul", { "class" : "product-list grid" }):
				for ul in soup.find_all( "ul", { "class" : "product-list grid" }):
					if ul.find_all( "a", { "class" : "product-image-wrapper" }):
						for a in ul.find_all( "a", { "class" : "product-image-wrapper" }):
							if a.get("href") :
								data = self.domains +  a.get("href") + "|" + response.meta['promotion_type']
								file.write(data)
								file.write("\n")
								self.count += 1
								print("========>"+str(self.count))
		except :
			file_error.write(response.url + "\n")


