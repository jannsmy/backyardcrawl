# -*- coding: utf-8 -*-

import os
import time
import datetime
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
import scrapy

class TescoSpider(Spider):
	
	name = "tesco"

	def __init__(self):
		self.domains = "https://shoponline.tescolotus.com"
		concurrent_requests = 10
		self.start_urls     = list()
		self.date     		= str(int(time.time()))
		self.all_product = list()
		
		self.start_urls     = [ "https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B8%AA%E0%B8%94,-%E0%B9%81%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%82%E0%B9%87%E0%B8%87-&-%E0%B9%80%E0%B8%9A%E0%B9%80%E0%B8%81%E0%B8%AD%E0%B8%A3%E0%B8%B5%E0%B9%88/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%81%E0%B8%AB%E0%B9%89%E0%B8%87-&-%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B8%81%E0%B8%A3%E0%B8%B0%E0%B8%9B%E0%B9%8B%E0%B8%AD%E0%B8%87/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%94%E0%B8%B7%E0%B9%88%E0%B8%A1,-%E0%B8%82%E0%B8%99%E0%B8%A1%E0%B8%82%E0%B8%9A%E0%B9%80%E0%B8%84%E0%B8%B5%E0%B9%89%E0%B8%A2%E0%B8%A7-&-%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B2%E0%B8%99/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B9%80%E0%B8%9E%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%AA%E0%B8%B8%E0%B8%82%E0%B8%A0%E0%B8%B2%E0%B8%9E-&-%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%87%E0%B8%B2%E0%B8%A1/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B8%AA%E0%B8%B3%E0%B8%AB%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B8%AA%E0%B8%B3%E0%B8%AB%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B8%AA%E0%B8%B1%E0%B8%95%E0%B8%A7%E0%B9%8C%E0%B9%80%E0%B8%A5%E0%B8%B5%E0%B9%89%E0%B8%A2%E0%B8%87/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B8%97%E0%B8%B3%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%AA%E0%B8%B0%E0%B8%AD%E0%B8%B2%E0%B8%94%E0%B9%83%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B8%B1%E0%B8%A7%E0%B9%80%E0%B8%A3%E0%B8%B7%E0%B8%AD%E0%B8%99/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B9%84%E0%B8%9F%E0%B8%9F%E0%B9%89%E0%B8%B2-&-%E0%B8%AD%E0%B8%B8%E0%B8%9B%E0%B8%81%E0%B8%A3%E0%B8%93%E0%B9%8C%E0%B8%A0%E0%B8%B2%E0%B8%A2%E0%B9%83%E0%B8%99%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B8%AA%E0%B8%B4%E0%B8%99%E0%B8%84%E0%B9%89%E0%B8%B2%E0%B8%AD%E0%B8%B7%E0%B9%88%E0%B8%99%E0%B9%86/all",
								"https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B9%80%E0%B8%97%E0%B8%A8%E0%B8%81%E0%B8%B2%E0%B8%A5/all" ]
		
		# self.start_urls     = [ "https://shoponline.tescolotus.com/groceries/th-TH/shop/%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B8%AA%E0%B8%B3%E0%B8%AB%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81/all"]
		self.count = 0
		file_promotion = open("./data/temp/tesco_promotion.txt", 'r')
		promotions = file_promotion.readlines()
		
		self.promotion_map = dict()
		for promotion in promotions:	
			data = promotion.split("|")
			self.promotion_map[data[0].strip()] = data[1].strip()

	def parse(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		if soup.find_all( "nav", { "class" : "pagination--page-selector-wrapper" }): 
			nav = soup.find_all( "nav", { "class" : "pagination--page-selector-wrapper" })[0]
			if nav.find_all( "li" ,{ "class" : "pagination-btn-holder" }):
				pages = set()
				pages.add(1)
				for li in nav.find_all( "li" ,{ "class" : "pagination-btn-holder" }) :
					if li.find_all( "span" ,{ "aria-hidden" : "true" }) :
						span = li.find_all( "span" ,{ "aria-hidden" : "true" })[0].get_text()
						try : 
							pages.add(int(span))
						except :
							pass
				#print(max(pages))
				for i in range(1, max(pages)+1):
					yield scrapy.Request(response.url + "?page=" + str(i), callback=self.get_link_product)
	
	
	def get_link_product(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		if soup.find_all( "ul", { "class" : "product-list grid" }):
			for ul in soup.find_all( "ul", { "class" : "product-list grid" }):
				if ul.find_all( "a", { "class" : "product-image-wrapper" }):
					for a in ul.find_all( "a", { "class" : "product-image-wrapper" }):
						path_product = a.get("href")
						yield scrapy.Request(self.domains + path_product, callback=self.get_product)

	def get_product(self, response):
		soup = BeautifulSoup(response.body, "lxml")
		product_id = response.url.split("/")[-1]
		product_name = ""
		group_product = ""
		main_category = ""
		sub_category = ""
		normal_price = ""
		normal_price_Unit = ""
		sub_price = ""
		sub_price_unit = ""
		promotion_price = ""
		promotion_price_unit = ""
		discount = ""
		promotion_date = ""
		regular_price = ""
		if self.promotion_map.get(response.url) :
			promotion_type = self.promotion_map.get(response.url)
			print("#################pro" + product_id)
			
		else :
			promotion_type = ""
		link = response.url
		sale_count = ""

		file_error = open("./data/temp/error/tesco_err.txt", 'a')
		#try :
		if soup.find_all( "span", { "class" : "beans-link__text styled__TextSpan-sc-1xizymv-3 hWdmzc" }):
			group_product = soup.find_all( "span", { "class" : "beans-link__text styled__TextSpan-sc-1xizymv-3 hWdmzc" })[3].get_text().encode('utf-8')
			main_category = soup.find_all( "span", { "class" : "beans-link__text styled__TextSpan-sc-1xizymv-3 hWdmzc" })[4].get_text().encode('utf-8')
			sub_category = soup.find_all( "span", { "class" : "beans-link__text styled__TextSpan-sc-1xizymv-3 hWdmzc" })[5].get_text().encode('utf-8')

		if soup.find_all( "h1", { "class" : "product-details-tile__title" }):
			product_name = soup.find_all( "h1", { "class" : "product-details-tile__title" })[0].get_text().encode('utf-8').strip()

		
		if soup.find_all( "div", { "class" : "list-item-content promo-content-small" }):
			div = soup.find_all( "div", { "class" : "list-item-content promo-content-small" })[0]
			if div.find_all( "span", { "class" : "offer-text" }):
				promotion_text = div.find_all( "span", { "class" : "offer-text" })[0].get_text().encode('utf-8')
				split_price = promotion_text.split("จากราคาปกติ  ")[1].split(" บาท ")
				regular_price = split_price[0]
				discount = split_price[1].replace("ประหยัด  ","").replace(" บาท","")
			if div.find_all( "span", { "class" : "dates" }):
				promotion_date = div.find_all( "span", { "class" : "dates" })[0].get_text().encode('utf-8')
				#promotion_date = dates.split("ราคานี้สำหรับสั่งซื้อและจัดส่งถึงวันที่ ")[1]
		
		if soup.find_all( "div", { "class" : "price-control-wrapper" }):
			div = soup.find_all( "div", { "class" : "price-control-wrapper" })[0]
			if div.find_all( "span", { "data-auto" : "price-value" }):
				sub_price = div.find_all( "span", { "data-auto" : "price-value" })[0].get_text().encode('utf-8')

		if soup.find_all( "div", { "class" : "price-per-quantity-weight" }):
			div = soup.find_all( "div", { "class" : "price-per-quantity-weight" })[0]
			if div.find_all( "span", { "data-auto" : "price-value" }):
				normal_price = div.find_all( "span", { "data-auto" : "price-value" })[0].get_text().encode('utf-8')
			if div.find_all( "span", { "class" : "weight" }):
				normal_price_unit = div.find_all( "span", { "class" : "weight" })[0].get_text().encode('utf-8').replace("/","")
		
		if sub_price != normal_price :
			if soup.find_all( "span", { "class" : "weightedProduct-text icon-chevron_down" }):
				sub_price_unit = soup.find_all( "span", { "class" : "weightedProduct-text icon-chevron_down" })[0].get_text().encode('utf-8')
		else : 
			sub_price = ""

		if regular_price != "":
			promotion_price = normal_price
			normal_price = regular_price
			promotion_price_unit = normal_price_unit

		file = open("./data/temp/tesco.txt", 'a')
		if product_id != "" and product_id not in self.all_product:
			data = ( product_id + "|" + product_name + "|" + group_product + "|" + main_category + "|" + sub_category + "|" + normal_price + "|" + normal_price_unit + "|" + sub_price + "|" + sub_price_unit + "|" + promotion_price + "|" + promotion_price_unit + "|" + discount + "|" + promotion_date + "|" + promotion_type + "|" + self.date + "|" + link + "|" + sale_count )
			self.count += 1
			print("========>"+str(self.count) + "     " + data)
			file.write(data)
			file.write("\n")
		else :
			file_error.write(response.url + "\n")
		'''
		except :
			file_error.write(response.url + "\n")
		'''