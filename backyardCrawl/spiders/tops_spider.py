# -*- coding: utf-8 -*-

import os
import json
import re
import csv
import time
import datetime
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
import scrapy

class TopsSpider(Spider):
	
	name = "tops"

	def __init__(self):
		allowed_domains     = ["https://www.tops.co.th"]		
		download_delay      = 0.50
		concurrent_requests = 10
		self.start_urls     = list()
		self.date     		= str(int(time.time()))
		self.start_urls     = [ "https://www.tops.co.th"]
		self.duplicate		= set()
		self.categorys      = {"43298" : "ผัก",	"43296" : "ข้าว", "43289" : "บ้าน", "43293" : "กัวเม", "43295" : "ผลไม้", "43276" : "หนังสือ/เครื่องเขียน และของเล่น",
								"43288" : "เบเกอรี่", "43286" : "อาหารทะเล", "43278" : "แอลกอฮอล์", "43272" : "อาหารแห้ง & อาหารกระป๋อง", "43287" : "เนื้อสัตว์",
								"43284" : "เครื่องดื่ม", "43291" : "เครื่องเขียน", "43277" : "อาหารแช่แข็ง", "43294" : "อาหารพร้อมทานและอาหารสะดวกซื้อ",
								"43274" : "ผลิตภัณฑ์จากนม ไข่ อาหารแช่เย็น", "43290" : "สุขภาพและความงาม", "43279" : "ผลิตภัณฑ์สำหรับเด็ก", "43283" : "ขนมขบเคี้ยวและของหวาน",
								"43297" : "ผลิตภัณฑ์ทำความสะอาดบ้าน", "43275" : "กีฬากลางแจ้งและการเดินทาง", "43281" : "ผลิตภัณฑ์สำหรับสัตว์เลี้ยง", "43285" : "ของใช้ในบ้านและผลิตภัณฑ์ทำความสะอาด",
								"52181" : "กระเช้าของขวัญ"}
	
	def parse(self, response):
		url = "https://www.tops.co.th/api/products?field=category.category_id%2C2230%2Ceq&filters=%7B%7D&page_number=3&page_size=20&sort=ranking%2Cdesc"
		yield scrapy.Request(url, callback = self.get_page, method = 'GET', 
		headers = {'accept': 'application/json, text/plain, */*',
		'x-store-code': 'tops_sa_432_th',
		'x-website-id' :'9'}) 
	
	def get_page(self, response):
		data = json.loads(response.body)
		print data
		'''
		page = data.get("meta").get("total_pages")
		if data.get("pages") != 0:
			for i in range(1, page + 1):
				url = response.url.split("&page=")[0] + "&page=" + str(i) + "&storeId=32821"
				print url
				yield scrapy.Request(url, callback = self.get_content, method = 'GET', headers = {'Accept': 'application/vnd.honestbee+json;version=2', 'Accept-Language':'th'}) 
		'''
	
	def get_content(self, response):		
		blank = ""
		data = json.loads(response.body)
		products = data.get("products")
		main_category = self.categorys.get(response.url.split("https://www.honestbee.co.th/api/api/departments/")[1].split("?sort")[0])
		if products != [] :
			for product in products :
				product_code = str(product.get("id"))
				if product_code not in self.duplicate :
					product_name = product.get("title").encode('utf-8')
					amount_per_unit = str(product.get("amountPerUnit"))
					try :
						size = product.get("size").encode('utf-8').strip()
					except :
						size = ""

					price = str(product.get("price"))
					normal_price = str(product.get("normalPrice")) 
					
					try :
						packing_size = product.get("packingSize").encode('utf-8').strip()
					except :
						packing_size = ''

					try :
						product_brand = product.get("productBrand").encode('utf-8')
					except :
						product_brand = ""
					file = open("./data/temp/honestBee.txt", 'a')
					d = product_code + "|" + product_name + "|" + main_category + "|" + blank + "|" + price + "|" + size + "|" + \
						blank + "|" + blank + "|" + blank + "|" + blank + "|" + self.date + "|" + blank + "|" + blank + "|" + \
						normal_price + "|" + amount_per_unit + "|" + product_brand + "|" + packing_size
					file.write(d)
					file.write("\n")
					self.duplicate.add(product_code)