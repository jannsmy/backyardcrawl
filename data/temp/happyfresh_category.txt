3|fresh-fruits-3/|ผลไม้สด|2|fresh-produce-2/|ผัก & ผลไม้
4|fresh-vegetables-4/|ผักสด|2|fresh-produce-2/|ผัก & ผลไม้
8|meat-counter-8/|เนื้อสัตว์ |7|meat-seafood-7/|เนื้อสัตว์ & อาหารทะเล
9|poultry-counter-9/|ไก่, เป็ด, สัตว์ปีก|7|meat-seafood-7/|เนื้อสัตว์ & อาหารทะเล
10|seafood-counter-10/|อาหารทะเล|7|meat-seafood-7/|เนื้อสัตว์ & อาหารทะเล
11|packaged-meat-seafood-11/|เนื้อสัตว์ & อาหารทะเลบรรจุแพ็ค|7|meat-seafood-7/|เนื้อสัตว์ & อาหารทะเล
16|prepared-meals-16/|อาหารพร้อมปรุง|13|deli-13/|อาหารสำเร็จรูป
17|fresh-dips-tapenades-17/|น้ำจิ้ม, น้ำพริก|13|deli-13/|อาหารสำเร็จรูป
18|tofu-meat-alternatives-18/|เต้าหู้ & โปรตีนเกษตร|13|deli-13/|อาหารสำเร็จรูป
131|deli-other-131/|อาหารสำเร็จรูป - อื่นๆ|13|deli-13/|อาหารสำเร็จรูป
20|bread-20/|ขนมปัง|19|bakery-19/|เบเกอรี่
22|buns-rolls-22/|ขนมปังนุ่มรสหวาน & ขนมปังกลม|19|bakery-19/|เบเกอรี่
23|bakery-desserts-23/|ขนมหวาน - อื่นๆ|19|bakery-19/|เบเกอรี่
132|bakery-other-132/|เบเกอรี่ - อื่นๆ |19|bakery-19/|เบเกอรี่
25|milk-25/|นม|24|dairy-eggs-24/|นมและไข่
26|cream-26/|ครีม |24|dairy-eggs-24/|นมและไข่
27|eggs-27/|ไข่|24|dairy-eggs-24/|นมและไข่
32|butter-32/|เนย|24|dairy-eggs-24/|นมและไข่
28|packaged-cheese-28/|ชีส |24|dairy-eggs-24/|นมและไข่
29|yogurt-29/|โยเกิร์ต|24|dairy-eggs-24/|นมและไข่
30|milk-alternatives-30/|ผลิตภัณฑ์ทดแทนนม|24|dairy-eggs-24/|นมและไข่
31|refrigerated-pudding-desserts-31/|พุดดิ้ง & ขนมหวาน|24|dairy-eggs-24/|นมและไข่
33|other-creams-cheeses-33/|ผลิตภัณฑ์จากครีมและชีสอื่นๆ|24|dairy-eggs-24/|นมและไข่
35|canned-fruit-applesauce-35/|ผลไม้กระป๋อง|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
36|canned-jarred-vegetables-36/|ผักบรรจุกระป๋อง & ขวดโหล|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
37|canned-meals-beans-37/|อาหาร & ถั่วกระป๋อง |34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
38|canned-meat-seafood-38/|เนื้อสัตว์ & อาหารทะเลกระป๋อง|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
39|soup-broth-bouillon-39/|ซุป, ซุปก้อน & ผงปรุงรส|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
40|dry-pasta-40/|เส้นแห้ง|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
41|fresh-pasta-41/|เส้นสด|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
42|pasta-sauce-42/|ซอสพาสต้า|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
43|instant-foods-43/|อาหารกึ่งสำเร็จรูป|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
44|grains-rice-dried-goods-44/|ธัญพืช, ข้าว & อาหารแห้ง|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
45|cereal-granola-45/|ซีเรียล & ข้าวโอ๊ต |34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
134|dry-canned-goods-other-134/|อาหารแห้ง & อาหารกระป๋อง - อื่นๆ|34|dry-canned-goods-34/|อาหารแห้ง & อาหารกระป๋อง
49|condiments-49/|เกลือ, น้ำตาล, ซอสต่างๆ|48|pantry-48/|เครื่องปรุง
50|honeys-syrups-nectars-50/|น้ำผึ้ง, น้ำเชื่อม, น้ำหวาน|48|pantry-48/|เครื่องปรุง
51|spices-seasonings-51/|เครื่องเทศ & เครื่องปรุงรส|48|pantry-48/|เครื่องปรุง
46|spreads-46/|อาหารที่ใช้ทาขนมปัง|48|pantry-48/|เครื่องปรุง
52|salad-dressing-toppings-52/|น้ำสลัด & เครื่องสลัดบาร์|48|pantry-48/|เครื่องปรุง
111|oils-vinegars-111/|น้ำมัน & น้ำส้มสายชู|48|pantry-48/|เครื่องปรุง
112|pickled-goods-olives-112/|อาหารดอง & มะกอกดอง|48|pantry-48/|เครื่องปรุง
113|marinades-meat-preparation-113/|ซอสหมัก & ซอสปรุงเนื้อ|48|pantry-48/|เครื่องปรุง
114|preserved-dips-spreads-114/|น้ำจิ้มและสเปรดทาขนมปัง|48|pantry-48/|เครื่องปรุง
115|doughs-gelatins-bake-mixes-115/|ส่วนผสม, แป้ง,วุ้น,ขนมอบ|48|pantry-48/|เครื่องปรุง
116|baking-ingredients-116/|ส่วนผสมทำขนมอบ|48|pantry-48/|เครื่องปรุง
117|baking-supplies-decor-117/|อุปกรณ์ทำและตกแต่งขนมอบ|48|pantry-48/|เครื่องปรุง
135|pantry-other-135/|เครื่องปรุง - อื่นๆ |48|pantry-48/|เครื่องปรุง
56|coffee-56/|กาแฟ|53|beverages-53/|เครื่องดื่ม
54|tea-54/|ชา|53|beverages-53/|เครื่องดื่ม
55|juices-health-drinks-55/|น้ำผลไม้ & เครื่องดื่มสุขภาพ|53|beverages-53/|เครื่องดื่ม
118|sport-soft-drinks-118/|น้ำอัดลม & เครื่องดื่มให้พลังงาน|53|beverages-53/|เครื่องดื่ม
58|water-seltzer-sparkling-water-58/|น้ำดื่ม & น้ำแร่|53|beverages-53/|เครื่องดื่ม
59|cocoa-drink-mixes-59/|โกโก้ & เครื่องดื่มผสม|53|beverages-53/|เครื่องดื่ม
136|beverages-other-136/|เครื่องดื่ม - อื่นๆ|53|beverages-53/|เครื่องดื่ม
61|candy-chocolate-61/|ลูกอม & ช็อคโกแลต|60|snacks-60/|ขนมขบเคี้ยว
62|chips-pretzels-62/|มันฝรั่งทอด & ขนมอบกรอบ|60|snacks-60/|ขนมขบเคี้ยว
63|cookies-cakes-63/|คุ้กกี้ & เค้ก|60|snacks-60/|ขนมขบเคี้ยว
64|crackers-64/|ขนมปังกรอบ|60|snacks-60/|ขนมขบเคี้ยว
65|energy-granola-bars-65/|ธัญพืชอัดแท่ง|60|snacks-60/|ขนมขบเคี้ยว
66|nuts-seeds-dried-snacks-66/|ถั่ว, เมล็ดธัญพืช & ขนมอบแห้ง|60|snacks-60/|ขนมขบเคี้ยว
67|trail-mix-snack-mix-67/|อาหารว่างจำพวกถั่วและผลไม้อบแห้ง|60|snacks-60/|ขนมขบเคี้ยว
68|mint-gum-68/|หมากฝรั่ง & ลูกอมมิ้นท์|60|snacks-60/|ขนมขบเคี้ยว
69|nutrition-bars-pastries-69/|อาหารอัดแท่ง & ขนมอบ|60|snacks-60/|ขนมขบเคี้ยว
137|snacks-other-137/|ขนมขบเคี้ยว - อื่นๆ|60|snacks-60/|ขนมขบเคี้ยว
71|frozen-appetizers-sides-71/|อาหารว่างแช่แข็ง|70|frozen-70/|อาหารแช่แข็ง
72|frozen-meals-72/|อาหารพร้อมทานแช่แข็ง|70|frozen-70/|อาหารแช่แข็ง
73|frozen-meat-seafood-73/|เนื้อสัตว์ & อาหารทะเลแช่แข็ง|70|frozen-70/|อาหารแช่แข็ง
74|frozen-breads-doughs-74/|ขนมปัง & แป้งแช่แข็ง|70|frozen-70/|อาหารแช่แข็ง
75|frozen-produce-75/|ผัก ผลไม้แช่แข็ง|70|frozen-70/|อาหารแช่แข็ง
77|frozen-dessert-77/|ของหวานแช่แข็ง|70|frozen-70/|อาหารแช่แข็ง
78|ice-cream-ice-78/|ไอศกรีม & น้ำแข็ง|70|frozen-70/|อาหารแช่แข็ง
90|baby-food-formula-90/|นม & อาหารผงสำหรับเด็ก|88|babies-88/|สำหรับทารก & เด็ก
89|baby-bath-body-care-89/|ผลิตภัณฑ์อาบน้ำ & บำรุงผิวสำหรับเด็ก|88|babies-88/|สำหรับทารก & เด็ก
91|diapers-wipes-91/|ผ้าอ้อมเด็ก & ผ้าเช็ดทำความสะอาด|88|babies-88/|สำหรับทารก & เด็ก
92|baby-first-aids-vitamins-92/|ชุดปฐมพยาบาลและวิตามินสำหรับเด็ก|88|babies-88/|สำหรับทารก & เด็ก
140|babies-other-140/|ผลิตภัณฑ์สำหรับเด็ก - อื่นๆ|88|babies-88/|สำหรับทารก & เด็ก
276|home-improvement-276/|อุปกรณ์ดูแลบ้าน|96|household-96/|ของใช้ในบ้าน
277|home-cleaning-277/|อุปกรณ์ทำความสะอาดบ้าน|96|household-96/|ของใช้ในบ้าน
100|laundry-100/|ผลิตภัณฑ์ซักผ้า|96|household-96/|ของใช้ในบ้าน
278|kitchen-utility-278/|เครื่องใช้ในครัว|96|household-96/|ของใช้ในบ้าน
281|car-care-281/|อุปกรณ์ดูแลรักษารถยนต์|96|household-96/|ของใช้ในบ้าน
283|plants-gardening-283/|อุปกรณ์ทำสวน|96|household-96/|ของใช้ในบ้าน
102|household-other-102/|ของใช้ในบ้าน - อื่นๆ|96|household-96/|ของใช้ในบ้าน
215|sushi-215/|อาหารพร้อมรับประทาน - อื่นๆ|212|ready-to-eat-212/|อาหารพร้อมรับประทาน
678|light-meals-678/|Light Meals|212|ready-to-eat-212/|อาหารพร้อมรับประทาน
679|refreshment-679/|Refreshment|212|ready-to-eat-212/|อาหารพร้อมรับประทาน
680|meals-680/|Meals|212|ready-to-eat-212/|อาหารพร้อมรับประทาน
681|ready-to-eat-other-681/|Ready to Eat - Other|212|ready-to-eat-212/|อาหารพร้อมรับประทาน
682|salads-682/|Salads|212|ready-to-eat-212/|อาหารพร้อมรับประทาน
403|recommendations-403/|Recommendations|213|top-picks-213/|สินค้ายอดนิยม
497|hamper-gift-set-497/|Hamper & Gift Set|259|festive-259/|สินค้าประจำเทศกาล
699|spirit-festival-699/|เทศกาลสารทจีน|259|festive-259/|สินค้าประจำเทศกาล
285|make-up-accessories-285/|เครื่องสำอางค์และอุปกรณ์แต่งหน้า|284|beauty-personal-care-284/|สุขภาพและความงาม
286|skin-hair-care-286/|บำรุงผมและผิว|284|beauty-personal-care-284/|สุขภาพและความงาม
287|bath-shower-287/|ผลิตภัณฑ์อาบน้ำ|284|beauty-personal-care-284/|สุขภาพและความงาม
288|oral-care-hygiene-288/|สุขภาพช่องปาก|284|beauty-personal-care-284/|สุขภาพและความงาม
289|feminine-care-289/|ผลิตภัณฑ์อนามัยสำหรับผู้หญิง|284|beauty-personal-care-284/|สุขภาพและความงาม
290|men-s-grooming-290/|ผลิตภัณฑ์สำหรับผู้ชาย|284|beauty-personal-care-284/|สุขภาพและความงาม
303|beauty-personal-care-other-303/|สุขภาพและความงาม - อื่นๆ|284|beauty-personal-care-284/|สุขภาพและความงาม
294|first-aid-health-care-294/|อุปกรณ์ปฐมพยาบาล|291|health-wellness-291/|ผลิตภัณฑ์เพื่อสุขภาพ
292|health-supplements-292/|อาหารเสริม|291|health-wellness-291/|ผลิตภัณฑ์เพื่อสุขภาพ
293|health-monitors-293/|อุปกรณ์สำหรับสุขภาพ|291|health-wellness-291/|ผลิตภัณฑ์เพื่อสุขภาพ
301|intimate-pleasure-301/|ผลิตภัณฑ์หล่อลื่นและถุงยางอนามัย|291|health-wellness-291/|ผลิตภัณฑ์เพื่อสุขภาพ
297|dog-food-care-297/|อาหารและผลิตภัณฑ์สำหรับสุนัข|296|pets-296/|สัตว์เลี้ยง
298|cat-food-care-298/|อาหารและผลิตภัณฑ์สำหรับแมว|296|pets-296/|สัตว์เลี้ยง
401|rum-401/|เหล้ารัม|384|rum-selection-384/|เหล้ารัม
410|non-alcoholic-beer-410/|เบียร์ไม่มีแอลกอฮอล์|397|beer-selection-397/|เบียร์
472|organic-fresh-produce-472/|ผัก ผลไม้ออร์แกนิค|471|organic-products-471/|ผลิตภัณฑ์ออร์แกนิค
473|organic-dairy-beverages-473/|นม เนย ไข่และเครื่องดื่มออร์แกนิค|471|organic-products-471/|ผลิตภัณฑ์ออร์แกนิค
475|organic-others-475/|ผลิตภัณฑ์ออร์แกนิค-อื่นๆ|471|organic-products-471/|ผลิตภัณฑ์ออร์แกนิค
486|beer-cider-486/|เบียร์และไซเดอร์|485|alcohol-485/|แอลกอฮอล์
487|cocktail-coolers-487/|คอกเทลและคูลเลอร์|485|alcohol-485/|แอลกอฮอล์
492|gin-vodka-tequila-492/|จิน วอดก้า และเตอกีล่า|485|alcohol-485/|แอลกอฮอล์
489|white-wines-489/|ไวน์ขาว|485|alcohol-485/|แอลกอฮอล์
490|red-wines-490/|ไวน์แดง|485|alcohol-485/|แอลกอฮอล์
491|other-wines-491/|ไวน์ชนิดอื่นๆ|485|alcohol-485/|แอลกอฮอล์
488|whiskey-rum-other-spirits-488/|วิสกี้ รัม และแอลกอฮอล์ชนิดอื่น|485|alcohol-485/|แอลกอฮอล์
981|new-981/|สินค้าใหม่|980|new-980/|สินค้าใหม่
1071|australian-goodness-1071/|Australian Goodness|1070|flavours-of-the-world-1070/|Flavours Of The World
1101|taste-of-japan-1101/|Taste of Japan|1070|flavours-of-the-world-1070/|Flavours Of The World
1117|taste-of-united-states-1117/|Taste of USA|1070|flavours-of-the-world-1070/|Flavours Of The World
953|discontinued-others-953/|Discontinued - Others|952|discontinued-952/|Discontinued
