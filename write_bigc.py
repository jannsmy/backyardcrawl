import os
import xlrd
import xlwt
import sys

f = open("./data/temp/bigc.txt", 'r')
s = f.readlines()

status_write_header = 1
book = xlwt.Workbook(encoding = "utf-8")
work_sheet = book.add_sheet("Sheet 1")

# Write header
i = 0
work_sheet.write(0, 0, "Product ID")
work_sheet.write(0, 1, "Product Name")
work_sheet.write(0, 2, "Main Category")
work_sheet.write(0, 3, "Sub Category")
work_sheet.write(0, 4, "Normal Price")
work_sheet.write(0, 5, "Unit")
work_sheet.write(0, 6, "Sub Price")
work_sheet.write(0, 7, "Unit")
work_sheet.write(0, 8, "Promotion Price")
work_sheet.write(0, 9, "Unit")
work_sheet.write(0, 10, "Discount")
work_sheet.write(0, 11, "Promotion Date")
work_sheet.write(0, 12, "Promotion Type")
work_sheet.write(0, 13, "Own Brand Big-C")
work_sheet.write(0, 14, "Date")
work_sheet.write(0, 15, "Link")
work_sheet.write(0, 16, "Sale Count")

index_row = 1
for i in s:
    try:
        a = i.split("|")
        if len(a) == 17:
            for i in range(0,len(a)):
                work_sheet.write(index_row, i, a[i])
            index_row += 1
        else : 
            print i
    except :
        print a
book.save("./data/bigc.xls")