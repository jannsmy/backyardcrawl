import os
import xlrd
import xlwt
import sys

#web = str(sys.argv[1])
f = open("./data/temp/honestBee.txt", 'r')
s = f.readlines()

status_write_header = 1
book = xlwt.Workbook(encoding = "utf-8")
work_sheet = book.add_sheet("Sheet 1")

# Write header
work_sheet.write(0, 0, "Product ID")
work_sheet.write(0, 1, "Product Name")
work_sheet.write(0, 2, "Main Category")
work_sheet.write(0, 3, "Sub Category")
work_sheet.write(0, 4, "Price")
work_sheet.write(0, 5, "Unit")
work_sheet.write(0, 6, "Sub Price")
work_sheet.write(0, 7, "Unit")
work_sheet.write(0, 8, "Promotion Price")
work_sheet.write(0, 9, "Unit")
work_sheet.write(0, 10, "Date")
work_sheet.write(0, 11, "Link")
work_sheet.write(0, 12, "Sale Count")
work_sheet.write(0, 13, "Normal Price") 
work_sheet.write(0, 14, "Amount Per Unit") 
work_sheet.write(0, 15, "Product Brand")
work_sheet.write(0, 16, "Packing Size")

index_row = 1
for i in s:
    ind = 0
    a = i.split("|")
    work_sheet.write(index_row, 0, a[0])
    work_sheet.write(index_row, 1, a[1])
    work_sheet.write(index_row, 2, a[2])
    work_sheet.write(index_row, 3, a[3])
    work_sheet.write(index_row, 4, a[4])
    work_sheet.write(index_row, 5, a[5])
    work_sheet.write(index_row, 6, a[6])
    work_sheet.write(index_row, 7, a[7])
    work_sheet.write(index_row, 8, a[8])
    work_sheet.write(index_row, 9, a[9])
    work_sheet.write(index_row, 10, a[10])
    work_sheet.write(index_row, 11, a[11])
    work_sheet.write(index_row, 12, a[12])
    work_sheet.write(index_row, 13, a[13])
    work_sheet.write(index_row, 14, a[14])
    work_sheet.write(index_row, 15, a[15])
    work_sheet.write(index_row, 16, a[16])
    index_row += 1

book.save("./data/honestBee.xls")