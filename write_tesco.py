import os
import xlrd
import xlwt


f = open("./data/temp/tesco.txt", 'r')
s = f.readlines()

status_write_header = 1
book = xlwt.Workbook(encoding = "utf-8")
work_sheet = book.add_sheet("Sheet 1")

# Write header
work_sheet.write(0, 0, "Product ID")
work_sheet.write(0, 1, "Product Name")
work_sheet.write(0, 2, "Group Product")
work_sheet.write(0, 3, "Main Category")
work_sheet.write(0, 4, "Sub Category")
work_sheet.write(0, 5, "Normal Price")
work_sheet.write(0, 6, "Unit")
work_sheet.write(0, 7, "Sub Price")
work_sheet.write(0, 8, "Unit")
work_sheet.write(0, 9, "Promotion Price")
work_sheet.write(0, 10, "Unit")
work_sheet.write(0, 11, "Discount")
work_sheet.write(0, 12, "Promotion Date")
work_sheet.write(0, 13, "Promotion Type")
work_sheet.write(0, 14, "Date")
work_sheet.write(0, 15, "Link")
work_sheet.write(0, 16, "Sale Count")

index_row = 1
for i in s:
    try:
        a = i.split("|")
        for i in range(0,len(a)):
            work_sheet.write(index_row, i, a[i])
        index_row += 1
    except :
        print a
book.save("./data/tesco.xls")